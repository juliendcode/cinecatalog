import { Component, OnInit } from '@angular/core';
import { Game } from '../game';
import { GameService } from '../game.service';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {
  games: Game[];
  // games = GAMES;
  // selectedGame: Game;

  // game: Game = {
  //   id: 1,
  //   name: 'Burnin Rubber'
  // };
  // game = 'Burnin Rubber'

  constructor(private gameService: GameService) {}

  ngOnInit() {
    this.getGames();
  }

  // onSelect(game: Game): void {
  //   this.selectedGame = game;
  // }
  getGames(): void {
    this.gameService.getGames().subscribe(games => (this.games = games));
  }

  add(name : string): void {
    name = name.trim();
    if (!name) { return; }
    this.gameService.addGame({ name } as Game)
      .subscribe(game => {
        this.games.push(game);
      });
  }

  delete(game : Game): void {
    this.games = this.games.filter(g => g !== game);
    this.gameService.deleteGame(game)
    .subscribe()
  }
}
