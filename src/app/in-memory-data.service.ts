import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Injectable } from '@angular/core';
import { Game } from './game';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const games = [
      { id: 11, name: 'Doom' },
      { id: 12, name: 'Ikari Warrior' },
      { id: 13, name: 'North & South' },
      { id: 14, name: 'Rick Dangerous' },
      { id: 15, name: 'Ghosts n Goblins' },
      { id: 16, name: 'Ghosts n Goblins' },
      { id: 17, name: 'Arkanaoid' }
    ];
    return { games };
  }
  genId(games: Game[]): number {
    return games.length > 0 ? Math.max(...games.map(game => game.id)) + 1 : 11;
  }
  constructor() {}
}
